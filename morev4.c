#include<stdio.h>
#include<stdlib.h>
#define PAGELEN 20
#define LINELEN 512
void do_more(FILE *);
int get_input(FILE*);
int  countLines(FILE*);
int main(int argc, int *argv[])
{
if (argc==1)
{ 
   do_more(stdin);
}
int i=0;
FILE *fp;
while(++i<argc)
{
fp= fopen(argv[i],"r");

  if (fp==NULL)
   {
      perror("Cant open the file");
      exit(1);
   }
  do_more(fp);
   fclose(fp);
   
}
return 0;
}

void do_more(FILE *fp)
{  int num_of_lines=0;
   int rv; 
    char buffer[LINELEN];
    int totalLines= countLines(fp);
    int  currentLine=0;
    printf("Lines %d",totalLines);
   FILE *fp_tty=fopen("/dev/tty","r");
 while (fgets(buffer,LINELEN,fp))
{
   fputs(buffer,stdout);
   
   currentLine++;
   num_of_lines++;
   if (num_of_lines== PAGELEN)
   {
   	
	   printf("\033[7m--more(%d %%)--\033[m",currentLine*100/totalLines);
          rv=get_input(fp_tty);
        if (rv==0) // user pressed q
          {   printf ("\033[1A \033[2K \033[1G");
                    break; 
          }
        else if (rv==1) // user pressed space bar
    {        printf ("\033[1A \033[2K \033[1G");
               num_of_lines -=PAGELEN;
    }
        else if (rv==2) // user presses enter
      { printf ("\033[1A \033[2K \033[1G");
       num_of_lines -=1; // show one more line
      }
        else if (rv==3) // invalid character
     {   printf ("\033[1A \033[2K \033[1G");
           break;
     }
   }

}

}

int get_input(FILE* cmdstream)
{
 int c;

  c=getc(cmdstream);
  if (c=='q')
   return 0;
  if (c==' ')
   return 1;
  if (c=='\n')
  return 2;
  return 3;
return 0;

 



}



int  countLines(FILE* fp){
	int lines=0; 
    char buffer[LINELEN];
  
		 while (fgets(buffer,LINELEN,fp))
		{
			lines++;
		}
		fseek(fp, 0L, SEEK_SET);
	return lines ;
}
